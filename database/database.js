const sqlite3 = require('sqlite3');

async function startApp() {
    console.log("hola");

    const db = new sqlite3.Database('./database/ima_database.db', sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE);

    const loginForm = document.getElementById('loginForm');
    loginForm.addEventListener('submit', handleLoginFormSubmit);

    async function handleLoginFormSubmit(event) {
        event.preventDefault();
        const username = document.getElementById('username').value;
        const password = document.getElementById('password').value;

        try {
            const user = await getUserByUsernameAndPassword(username, password);
            if (user) {
                console.log('Inicio de sesión exitoso.');
                // Handle successful login
            } else {
                console.log('Credenciales incorrectas.');
                // Handle incorrect credentials
            }
        } catch (err) {
            console.error(err.message);
            // Handle database errors
        }
    }

    async function getUserByUsernameAndPassword(username, password) {
        const query = 'SELECT * FROM users WHERE username = ? AND password = ?';
        const user = await new Promise((resolve, reject) => {
            db.get(query, [username, password], (err, row) => {
                if (err) reject(err);
                else resolve(row);
            });
        });
        return user;
    }
}

module.exports = {
    startApp,
};




ipcMain.handle('test', (event, msg) => {
    console.log(msg);
    return 'Message received on main process!';
});
