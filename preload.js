const { ipcRenderer } = require('electron');

window.addEventListener('DOMContentLoaded', () => {
    ipcRenderer.send('getSystemInfo');

    ipcRenderer.on('systemInfo', (event, systemInfo) => {
        console.log(systemInfo);
    });

    const startButton = document.getElementById('startButton');
    if (startButton) {
        startButton.addEventListener('click', startApp);
    }

});

async function startApp() {
  const username = document.getElementById('username').value;
  const password = document.getElementById('password').value;

  try {
      const user = await ipcRenderer.invoke('test', username, password);
      if (user) {
        console.log('Inicio de sesión exitoso.');
        console.log(user);
        ipcRenderer.send('close-window');
      } else {
          console.log('Credenciales incorrectas.');
      }
  } catch (error) {
      console.error('Error invoking test:', error);
  }
}