const { app, BrowserWindow, Tray, Menu, ipcMain } = require('electron');
const sqlite3 = require('sqlite3');
const path = require('path');
const os = require('os');




const { promisify } = require('util');
const db = new sqlite3.Database('./database/ima_database.db', sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE);
const dbGetAsync = promisify(db.get.bind(db));


let win = null; // Declarar win de manera global

function createMainWindow() {
    const win = new BrowserWindow({
        width: 600,
        height: 800,
        autoHideMenuBar: true,
        webPreferences: {
            preload: path.join(__dirname, 'preload.js')
        }
    });

    win.on('close', (e) => {
        if (win.isMinimized()) {
            e.preventDefault();
            win.minimize();
        } else {
            // Comentar la siguiente línea para permitir cerrar la aplicación
            e.preventDefault();
        }
    });
   

    ipcMain.on('close-window', () => {
        if (win) {
            win.destroy(); // Cerrar la ventana principal
        }
    });

    win.setSkipTaskbar(true)
    win.loadFile('index.html');
}

let tray = null;

app.whenReady().then(() => {
    tray = new Tray(path.join(__dirname, './public/img/tray-icon-2.jpeg'));

    const contextMenu = Menu.buildFromTemplate([
        { label: 'Abrir', click: createMainWindow },
        { label: 'Version', click: () => console.log(app.getVersion()) },
        { label: 'Salir', click: () => app.quit() }
    ]);

    tray.setToolTip('Esta es mi aplicación.');
    tray.setContextMenu(contextMenu);

    createMainWindow();

    app.on('activate', () => {
        if (BrowserWindow.getAllWindows().length === 0) {
            createMainWindow();
        }
    });
});

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.disableHardwareAcceleration(); //desactiva los mensajes de errores del gpu o grafica.










ipcMain.on('getSystemInfo', (event) => {
    const systemInfo = {
        platform: os.platform(),
        release: os.release(),
        cpus: os.cpus() || [],
        totalmem: os.totalmem(),
        networkInterfaces: os.networkInterfaces() || {},
        hostname: os.hostname(),
    };

    event.reply('respuestaGetSystemInfo', systemInfo);

    const systemInfox = JSON.stringify(systemInfo);

    if (systemInfoCallback) {
        systemInfoCallback(systemInfox);
    }
});

systemInfoCallback = async (systemInfox) => {
    try {
        if (!systemInfox) {
            throw new Error('Todos los campos son obligatorios');
        }

        systemInfox = JSON.parse(systemInfox);

        const row = await dbGetAsync("SELECT * FROM systemInfo");
        console.log('PASO 1 _ NO HAY DATA');

        if (row) {
            
            console.log('PASO 2 _ Resultado de la consulta:', row.id);
            const updateResult = await new Promise((resolve, reject) => {
                db.run(
                    'UPDATE systemInfo SET platform = ?, release = ?, cpus = ?, totalmem = ?, networkInterfaces = ?, hostname = ? WHERE id = ?',
                    [systemInfox.platform, systemInfox.release, JSON.stringify(systemInfox.cpus || []), systemInfox.totalmem, JSON.stringify(systemInfox.networkInterfaces || {}), systemInfox.hostname, row.id],
                    (err) => {
                        if (err) {
                            console.error('Error updating systemInfo:', err);
                            reject(err);
                        } else {
                            resolve('Usuario actualizado');
                        }
                    }
                );
            });
            console.log("PASO 3 _ ",updateResult);
        } else {
            const query = `INSERT INTO systemInfo (platform, release, cpus, totalmem, networkInterfaces, hostname) VALUES (?, ?, ?, ?, ?, ?)`;
            
            db.run(query, [ systemInfox.platform, systemInfox.release, JSON.stringify(systemInfox.cpus || []), systemInfox.totalmem,JSON.stringify(systemInfox.networkInterfaces || {}), systemInfox.hostname ], function (error) { 
                if (error) {
                    console.error('Error al realizar la inserción:', error); 
                } else {
                    console.log('PASO 4 _ Inserción exitosa. ID del nuevo registro:', this.lastID);
                }
            });
        }

    } catch (error) {
        console.error("Error en systemInfoCallback:", error);
    }
};











//LOGIN 
ipcMain.handle('test', async (event, username, password) => {
    try {
        // Abre la conexión a la base de datos
        const db = new sqlite3.Database('./database/ima_database.db', sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE);

        // Realiza la consulta a la base de datos
        const query = 'SELECT * FROM users WHERE username = ? AND password = ?';
        const user = await new Promise((resolve, reject) => {
            db.get(query, [username, password], (err, row) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(row);
                }
            });
        });

        // Cierra la conexión a la base de datos
        db.close();

        //console.log(username, password);

        // Devuelve el resultado de la consulta
        return user;
    } catch (error) {
        console.error(error.message);
        // Manejar el error, podrías enviar un mensaje al proceso de renderizado
        throw error;
    }
});




async function main() {
    const db = new sqlite3.Database('./database/ima_database.db', sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE);
  
    await db.exec(`
    CREATE TABLE users (
        id int primary key not null,
        username text not null,
        password text not null
      );      
    `);
  
  
    await db.exec(`
    CREATE TABLE IF NOT EXISTS systemInfo (
        id INTEGER PRIMARY KEY,
        platform TEXT,
        release TEXT,
        cpus TEXT,
        totalmem TEXT,
        networkInterfaces TEXT,
        hostname TEXT,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        user_id INTEGER,
        FOREIGN KEY (user_id) REFERENCES users(id)
      );  
    `);
  
    // Inserta datos en las tablas de forma asíncrona
    await db.exec(`
      insert into users (id, username, password)
      values (1, 'Spiderman', '123'),
      (2, 'TonyStark', '123'),
      (3, 'admin', '123'),
      (4, 'JeanGrey', '123');      
    `);
  /*
      // Inserta datos en las tablas de forma asíncrona
      await db.exec(`
      insert into systemInfo (id, os_version, cpu_info, ram_size, user_id)
      values (1, '1.1.1.1', '2222222', '16', '1'),
      (2, '1.1.1.1', '9999', '16', '1'),
      (3, '1.1.1.1', '111', '8', '1'),
      (4, '1.1.1.1', '4444', '4', '2'),
      (5, '1.1.1.1', '333', '32', '2'),
      (6, '1.1.1.1', '6666', '64', '3'),
      (7, '1.1.1.1', '555', '128', '4'),
      (8, '1.1.1.1', '777', '256', '2'),
      (9, '1.1.1.1', '88888', '2', '4');
    `);
    */
  
    // Cierra la conexión a la base de datos
    await db.close();
  }
  
//main();